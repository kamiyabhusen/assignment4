import React, { useEffect} from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'

//routes
import PrivateRoute from './components/routing/PrivateRoute';


//Redux
import store from './store';
import setAuthToken from './utils/setAuthToken';
import { loadUser } from './actions/user'

//components
import AlertBox from './components/AlertBox';

//Pages
import RegisterPage from './pages/Register';
import LoginPage from './pages/Login';
import HomePage from './pages/Home';
import PackagePage from './pages/PackagesList';

//layout
import AdminLayout from './components/layout/adminLayout';

//Css
import "./assets/css/argon-dashboard-react.min.css";
import "@fortawesome/fontawesome-free/css/all.min.css";
import './App.css';

function App() {

  useEffect(() => {
    
    // check for token in LS
    if (localStorage.token) {
      //setAuth token in axios header
      setAuthToken(localStorage.getItem('token'));
    }

    //load the user from backend
    store.dispatch(loadUser());
  }, []);

  return (
    <Provider store={store}>
      <Router>
        <AlertBox />
        <Switch>
          <Route path="/admin" render={props => <AdminLayout {...props} />} />
          <Route path="/register" exact component={RegisterPage} />
          <Route path="/login" exact component={LoginPage} />
          <PrivateRoute path="/packages" exact component={PackagePage} /> 
          <PrivateRoute path="/" exact  component={HomePage} /> 
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
