import api from '../utils/api';
import { authSuccess,authFail,userLoaded,authStart } from '../slices/userSlices';
import {alert} from './alert';
import setAuthToken from '../utils/setAuthToken';


/**
 * Register Action for registering User with Data
 *  
 */
export const register = (data) => async dispatch => {

    //set loading to true in user state indicate autherization start
    dispatch(authStart());
    try {
        
        //Request to Backend for creating user
        const res = await api.post("/users",{
           ...data,
            typeOfUser:"business"
        });
        let resData = res.data;
        localStorage.setItem("token",resData.token);

        //set Token to Axios Header
        setAuthToken(resData.token);

        //dispatching authSuccess action if api return 200
        dispatch(authSuccess({
            username:resData.username,
            typeOfUser:resData.typeOfUser
        }))

        //dispatching alert action for displaying alert in browser
        dispatch(alert("Register Successfully","success"));

    } catch (error) {
        console.log(error.response)
        let msg = error.response.data.error;
        dispatch(alert(msg,'danger'));
    }

}


/**
 * LOgin Action for Logining User with Data
 *  
 */
export const login = (data) => async dispatch => {

    //set loading to true in user state indicate autherization start
    dispatch(authStart());
    try {
            
        const res = await api.post("/users/login",data);
        let resData = res.data;

        localStorage.setItem("token",resData.token);
        setAuthToken(resData.token);

        dispatch(authSuccess({
            username:resData.username,
            typeOfUser:resData.typeOfUser
        }))
        dispatch(alert("login Successfully","success"));

    } catch (error) {
        console.log(error.response)
        let msg = error.response.data.error;
        dispatch(alert(msg,'danger'));
    }

}

/**
 * Logout action
 *  
 */
export const logout = () => async dispatch => {
    
    localStorage.removeItem("token");
    dispatch(authFail());
  };

/**
 * LoadUser action for loading user in the app if token is available
 *  
 */
export const loadUser = () => async dispatch => {
    //set loading to true in user state indicate autherization start
    dispatch(authStart());
    try {
      const res = await api.get('/users/me');

      let resData = res.data;

      dispatch(userLoaded(resData));

    } catch (err) {
        dispatch(authFail());
    }
  };