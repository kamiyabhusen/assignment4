import { v4 as uuidv4 } from 'uuid';
import {setAlert,removeAlert} from '../slices/alertSlices';


//Alert Action
export const alert = (msg, alertType, timeout=5000) => dispatch => {
    const id = uuidv4();
   dispatch(setAlert({
       id,
       msg, 
       alertType
   }));

   setTimeout(() => {
        dispatch(removeAlert(id))
   },timeout)
}