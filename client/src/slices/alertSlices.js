import { createSlice } from '@reduxjs/toolkit';

//Alert slices
const alert = createSlice({
    name:"alert",
    initialState:{
        alerts:[]
    },
    reducers:{
        setAlert:(state,action) => {
            state.alerts = [...state.alerts,action.payload];
        },
        removeAlert:(state,action) => {
            state.alerts = state.alerts.filter((alert) => alert.id !== action.payload);
        },
    }
});

export default alert.reducer;

export const { setAlert, removeAlert } = alert.actions;