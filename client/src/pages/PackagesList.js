import React, { useEffect, useState } from 'react'
import { useSelector,useDispatch } from 'react-redux';
import { 
    Button,
    Card,
    CardBody,
    CardText,
    CardTitle,
    Col,
    Container, Row 
} from 'reactstrap';
import api from '../utils/api';
import { alert } from '../actions/alert';
import { logout  } from '../actions/user'

export default function PackagesList() {
    const [packages,setPackages] = useState([]);

    const dispatch = useDispatch();


    useEffect(() => {
        
        (async () => {
            try {
                const res = await api.get("/packages");
                const resData = res.data.packages;
                console.log(res.data);
                setPackages(resData);
            } catch (error) {
                console.log(error.response)
                let msg = error.response.data.error;
                dispatch(alert(msg,'danger'));
            }
        })()

    },[]);

    const handleLogout = () => {
        dispatch(logout());
    }
    
    return (
        <Container>
            <Row  className="justify-content-md-center mt-5">
                <Col md="6">
                    <h1 className="display-2 text-center"> Available Packages</h1>
                   {packages.map((pack) => (
                        <Card className="mb-2">
                            <CardBody>
                            <CardTitle className="text-uppercase font-weight-bold  mb-0">
                                {pack.name}
                            </CardTitle>
                            <span className="text-muted mb-0">Price : {pack.price}</span>
                            </CardBody>
                        </Card>
                       )
                   )}
                   <div className="mt-5 text-center">
                        <Button onClick={handleLogout} color="primary" type="button">
                            Logout
                        </Button>
                    </div>
                </Col>
            </Row>
        </Container>
    )
}