import React, { useEffect, useState } from 'react'
import { useSelector,useDispatch } from 'react-redux';
import { 
    Button,
    Card,
    CardBody,
    CardText,
    CardTitle,
    Col,
    Container, Row 
} from 'reactstrap';
import api from '../utils/api';
import { alert } from '../actions/alert';
import { logout  } from '../actions/user'
import { Redirect } from 'react-router-dom';

export default function Home() {
    
    const [userPackage,setUserPackage] = useState(null);
    const [redirect,setRedirect] = useState(false);
    
    const { user } = useSelector(state => state.user);
    const dispatch = useDispatch();


    useEffect(() => {
        (async () => {
            try {
                const res = await api.get("/users/package");
                const resData = res.data;
                setUserPackage(resData);
            } catch (error) {
                console.log(error.response)
                if(error.response.status === 400){
                    setRedirect(true);
                }
                let msg = error.response.data.error;
                dispatch(alert(msg,'danger'));
            }
        })()

    },[]);

    const calculateExpireDate = (date) => {
        let newDate = new Date(date);
        newDate.setDate(newDate.getDate() + 14);

        return `${newDate.getDate()} / ${newDate.getMonth()+1} / ${newDate.getFullYear()}`;
    }

    const handleLogout = () => {
        dispatch(logout());
    }


    //if pack is expired redirect to packages pages
    if(redirect){
        return <Redirect to="/packages" />
    }
    
    return (
        <Container>
            <Row  className="justify-content-md-center mt-5">
                <Col md="6">
                    <h1 className="display-2 text-center"> Hello <strong>{user ? user.username : ""}</strong></h1>
                    <Card>
                        <CardBody>
                            <CardTitle className="display-4">Package Information</CardTitle>
                            <CardText>
                                { userPackage && (
                                        <>
                                            <h1> Name : <strong>{userPackage.name}</strong></h1>
                                            <h1> Expire Date : <strong>{calculateExpireDate(userPackage.createdAt)}</strong></h1>
                                        </>
                                    )

                                }
                            </CardText>
                        </CardBody>
                    </Card>
                    <div className="mt-5 text-center">
                        <Button onClick={handleLogout} color="primary" type="button">
                            Logout
                        </Button>
                    </div>
                </Col>
            </Row>
        </Container>
    )
}
