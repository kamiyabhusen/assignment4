import React,{useState,useEffect} from 'react'
import{
    Button,
    Card,
    CardFooter,
    CardHeader,
    Col,
    Row,
    Table
} from 'reactstrap';
import { NavLink as NavLinkRRD } from 'react-router-dom'
import { useDispatch } from 'react-redux';
import { alert } from '../../actions/alert';
import UsersTableBody from '../../components/UsersTableBody';
import api from '../../utils/api';
import CustomPagination from '../../components/CustomPagination';
import { getDefaultMiddleware } from '@reduxjs/toolkit';

export default function Users() {

    const [users,setusers] = useState([]);
    const [page,setPage] = useState(1);
    const [perPage,setPerPage] = useState(5);
    const [totalRecords,setTotalRecords] = useState(0);

    const dispatch = useDispatch();

    //get users when components first render
    useEffect(() => {    
      getData();
    },[]);

    //get users when components updates page
    useEffect(() => {
      getData();
   }, [page])


    const getData = async () => {
      console.log("hello");
      try {
          const res = await api.get(`/users?page=${page}&perpage=${perPage}`);
          const resData = res.data.users;
          setusers(resData);
          setTotalRecords(res.data.total);
      } catch (error) {
          console.log(error)
          let msg = error.response.data.error;
          dispatch(alert(msg,'danger'));
      }
  };

    const handleDelete =  async (id) => {
        try {
            
            
            const res = await api.delete("/users/"+id);
            dispatch(alert("user Deleted successfully",'success'));

            setusers(users.filter(user => user.id !== id));
        } catch (error) {
            let msg = error.response.data.error;
            dispatch(alert(msg,'danger'));
        }
    }
    const chnagePage = (page) => {
      setPage(page);
    }
 

    return (
        <Row className="mt-4">
          <div className="col">
            <Card className="shadow">
              <CardHeader className="border-0">
                <Row className="align-items-center">
                    <Col xs="8">
                      <h3 className="mb-0">All Users</h3>
                    </Col>
                    <Col className="text-right" xs="4">
                      <Button
                        color="primary"
                        to="/admin/users/add"
                        tag={NavLinkRRD}
                        size="md"
                      >
                        Add New
                      </Button>
                    </Col>
                  </Row>
              </CardHeader>
              <Table
                className="align-items-center table-flush"
                style={{ minHeight: "200px" }}
                responsive
              >
                <thead className="thead-light">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Username</th>
                    <th scope="col">Email</th>
                    <th scope="col">typeOfUser</th>
                    <th scope="col">Status</th>
                    <th scope="col">Package</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {users.map((user,index) => {
                    let i = index + ( perPage * ( page - 1 ) ) + 1;
                    return <UsersTableBody handleDelete={handleDelete} index={i} user={user} />;
                  })}
                </tbody>
              </Table>
              <CardFooter className="py-4">
                <CustomPagination
                  itemPerPage={perPage}
                  page={page}
                  totalRecords={totalRecords}
                  chnagePage={chnagePage}
                />
              </CardFooter>
            </Card>
          </div>
        </Row>
    )
}
