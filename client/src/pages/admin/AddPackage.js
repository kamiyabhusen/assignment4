import React, { useState } from 'react'

import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    Form,
    FormGroup,
    Input,
    Button,
  } from "reactstrap";
import { useDispatch } from 'react-redux';
import { alert } from '../../actions/alert';
import api from '../../utils/api';
import { Redirect } from 'react-router-dom';

export default function AddUser() {
    const [name,setName] = useState("");
    const [price,setprice] = useState(0);
    const [status,setStatus] = useState(true);
    const [redirect,setRedirect] = useState(false);

    const dispatch = useDispatch();

    const handleFormSubmit = async (e) => {
        e.preventDefault();

        try {
            
            let userStatus = 'active'
            if(!status)
                userStatus = 'inactive';

            
            const res = await api.post("/packages",{
                name,
                price,
                status:userStatus
            });
            dispatch(alert("package Created successfully",'success'));
           setRedirect(true);
        } catch (error) {
            let msg = error.response.data.error;
            dispatch(alert(msg,'danger'));
        }
    };

    //refirect to /admin/packages page after successfull response
    if(redirect){
        return <Redirect to="/admin/packages" />
    }

    return (
        <Row className="mt-4">
        <Col>
          <Card className="bg-secondary shadow">
            <CardHeader className="bg-white border-0">
              <h3 className="mb-0">Add Package</h3>
            </CardHeader>
            <CardBody>

              <Form className="px-lg-5" onSubmit={handleFormSubmit}>
                <FormGroup>
                  <label className="form-control-label" htmlFor="input-name">
                    Name
                  </label>
                  <Input
                    className="form-control-alternative"
                    placeholder="Name"
                    type="text"
                    name="name"
                    required
                    onChange={(e) => setName(e.target.value)}
                  />
                </FormGroup>
                <FormGroup>
                  <label className="form-control-label" htmlFor="input-price">
                    Price
                  </label>
                  <Input
                    className="form-control-alternative"
                    placeholder="Price"
                    type="number"
                    name="price"
                    required
                    onChange={(e) => setprice(e.target.value)}
                  />
                </FormGroup>
                <FormGroup>
                  <label
                    className="form-control-label"
                    htmlFor="input-status"
                  >
                    Status
                  </label>
                  <br />
                  <label className="custom-toggle">
                    <input
                      type="checkbox"
                      defaultChecked
                      name="status"
                      onChange={() => setStatus(!status)}
                    />
                    <span className="custom-toggle-slider rounded-circle"></span>
                  </label>
                </FormGroup>
                <Button color="primary" type="submit">
                  Submit
                </Button>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    )
}

