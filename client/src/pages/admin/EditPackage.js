import React, { useState, useEffect } from 'react'

import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    Form,
    FormGroup,
    Input,
    Button,
  } from "reactstrap";
import { useDispatch } from 'react-redux';
import { alert } from '../../actions/alert';
import api from '../../utils/api';
import { Redirect, useParams  } from 'react-router-dom';

export default function EditPackage() {

    const { id } = useParams();

    const [name,setName] = useState("");
    const [price,setprice] = useState(0);
    const [status,setStatus] = useState(true);
    const [redirect,setRedirect] = useState(false);

    const dispatch = useDispatch();


    useEffect(() => {
        
        (async () => {
            try {
                //get Package Details
                const res = await api.get("/packages/"+id);
                const resData = res.data.package;
                console.log(res.data);
                setName(resData.name);
                setprice(resData.price);
                let userStatus = true;
                if(resData.status === "inactive"){
                    userStatus = false;
                }
                setStatus(userStatus);
            } catch (error) {
                console.log(error);
                let msg = error.response.data.error;
                dispatch(alert(msg,'danger'));
            }
        })()

    },[]);


    const handleFormSubmit = async (e) => {
        e.preventDefault();

        try {
            
            let userStatus = 'active'
            if(!status)
                userStatus = 'inactive';

            
            const res = await api.put("/packages/"+id,{
                name,
                price,
                status:userStatus
            });
            dispatch(alert("package Updated successfully",'success'));
           setRedirect(true);
        } catch (error) {
            let msg = error.response.data.error;
            dispatch(alert(msg,'danger'));
        }
    };

    if(redirect){
        return <Redirect to="/admin/packages" />
    }

    return (
        <Row className="mt-4">
        <Col>
          <Card className="bg-secondary shadow">
            <CardHeader className="bg-white border-0">
              <h3 className="mb-0">Add User</h3>
            </CardHeader>
            <CardBody>

              <Form className="px-lg-5" onSubmit={handleFormSubmit}>
                <FormGroup>
                  <label className="form-control-label" htmlFor="input-name">
                    Name
                  </label>
                  <Input
                    className="form-control-alternative"
                    placeholder="Name"
                    type="text"
                    name="name"
                    value={name}
                    required
                    onChange={(e) => setName(e.target.value)}
                  />
                </FormGroup>
                <FormGroup>
                  <label className="form-control-label" htmlFor="input-price">
                    Price
                  </label>
                  <Input
                    className="form-control-alternative"
                    placeholder="Price"
                    type="number"
                    name="price"
                    value={price}
                    required
                    onChange={(e) => setprice(e.target.value)}
                  />
                </FormGroup>
                <FormGroup>
                  <label
                    className="form-control-label"
                    htmlFor="input-status"
                  >
                    Status
                  </label>
                  <br />
                  <label className="custom-toggle">
                    <input
                      type="checkbox"
                      checked={status}
                      name="status"
                      onChange={() => setStatus(!status)}
                    />
                    <span className="custom-toggle-slider rounded-circle"></span>
                  </label>
                </FormGroup>
                <Button color="primary" type="submit">
                  Update
                </Button>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    )
}

