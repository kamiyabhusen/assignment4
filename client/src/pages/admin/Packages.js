import React,{useState,useEffect} from 'react'
import{
    Button,
    Card,
    CardFooter,
    CardHeader,
    Col,
    Row,
    Table
} from 'reactstrap';
import { NavLink as NavLinkRRD } from 'react-router-dom'
import { useDispatch } from 'react-redux';
import { alert } from '../../actions/alert';
import PackagesTableBody from '../../components/PackagesTableBody';
import api from '../../utils/api';
import CustomPagination from '../../components/CustomPagination';

export default function Users() {

    const [packages,setPackages] = useState([]);
    const [page,setPage] = useState(1);
    const [perPage,setPerPage] = useState(5);
    const [totalRecords,setTotalRecords] = useState(0);

    const dispatch = useDispatch();

    //get Packages when components first render
    useEffect(() => {
      getData()
    },[]);

    //get Packages when components update page state
    useEffect(() => {
      getData();
   }, [page])

    const getData = async () => {
      try {
          const res = await api.get(`/packages?page=${page}&perpage=${perPage}`);
          const resData = res.data.packages;
          setPackages(resData);
          setTotalRecords(res.data.total);
      } catch (error) {
          console.log(error)
          let msg = error.response.data.error;
          dispatch(alert(msg,'danger'));
      }
  };

    const handleDelete =  async (id) => {
        try {
            
            
            const res = await api.delete("/packages/"+id);
            dispatch(alert("Package Deleted successfully",'success'));

            setPackages(packages.filter(pack => pack.id !== id));
        } catch (error) {
            let msg = error.response.data.error;
            dispatch(alert(msg,'danger'));
        }
    }

    const chnagePage = (page) => {
      setPage(page);
    }

    return (
        <Row className="mt-4">
          <div className="col">
            <Card className="shadow">
              <CardHeader className="border-0">
                <Row className="align-items-center">
                    <Col xs="8">
                      <h3 className="mb-0">All Packages</h3>
                    </Col>
                    <Col className="text-right" xs="4">
                      <Button
                        color="primary"
                        to="/admin/packages/add"
                        tag={NavLinkRRD}
                        size="md"
                      >
                        Add New
                      </Button>
                    </Col>
                  </Row>
              </CardHeader>
              <Table
                className="align-items-center table-flush"
                style={{ minHeight: "200px" }}
                responsive
              >
                <thead className="thead-light">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Price</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {packages.map((pack,index) => {
                    return <PackagesTableBody handleDelete={handleDelete} index={index+1} pack={pack} />;
                  })}
                </tbody>
              </Table>
              <CardFooter className="py-4">
                <CustomPagination
                  itemPerPage={perPage}
                  page={page}
                  totalRecords={totalRecords}
                  chnagePage={chnagePage}
                />
              </CardFooter>
            </Card>
          </div>
        </Row>
    )
}
