import React, { useState } from 'react'

import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    Form,
    FormGroup,
    Input,
    Button,
  } from "reactstrap";
import { useDispatch } from 'react-redux';
import { alert } from '../../actions/alert';
import api from '../../utils/api';
import { Redirect } from 'react-router-dom';

export default function AddUser() {
    const [username,setUsername] = useState("");
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");
    const [typeOfUser,setTypeOfUser] = useState("business");
    const [status,setStatus] = useState(true);
    const [redirect,setRedirect] = useState(false);

    const dispatch = useDispatch();

    const handleFormSubmit = async (e) => {
        e.preventDefault();

        try {
            
            let userStatus = 'active'
            if(!status)
                userStatus = 'inactive';

            
            const res = await api.post("/users",{
                username,
                email,
                password,
                typeOfUser,
                status:userStatus
            });
            dispatch(alert("user Created successfully",'success'));
           setRedirect(true);
        } catch (error) {
            let msg = error.response.data.error;
            dispatch(alert(msg,'danger'));
        }
    };

    if(redirect){
        return <Redirect to="/admin/users" />
    }

    return (
        <Row className="mt-4">
        <Col>
          <Card className="bg-secondary shadow">
            <CardHeader className="bg-white border-0">
              <h3 className="mb-0">Add User</h3>
            </CardHeader>
            <CardBody>

              <Form className="px-lg-5" onSubmit={handleFormSubmit}>
                <FormGroup>
                  <label className="form-control-label" htmlFor="input-username">
                    Username
                  </label>
                  <Input
                    className="form-control-alternative"
                    placeholder="Username"
                    type="text"
                    name="username"
                    required
                    onChange={(e) => setUsername(e.target.value)}
                  />
                </FormGroup>
                <FormGroup>
                  <label className="form-control-label" htmlFor="input-email">
                    Email
                  </label>
                  <Input
                    className="form-control-alternative"
                    placeholder="Email"
                    type="email"
                    name="email"
                    required
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </FormGroup>
                <FormGroup>
                  <label className="form-control-label" htmlFor="input-password">
                    Password
                  </label>
                  <Input
                    className="form-control-alternative"
                    placeholder="Password"
                    type="password"
                    name="password"
                    required
                    onChange={(e) => setPassword(e.target.value)}
                  />
                </FormGroup>
                <FormGroup>
                    <label
                      className="form-control-label"
                      htmlFor="input-category"
                    >
                      User Type
                    </label>
                    <Input
                      type="select"
                      onChange={(e) => setTypeOfUser(e.target.value)}
                      className="form-control-alternative"
                      name="typeOfUser"
                    >
                      <option value="business">business</option>
                      <option value="main">main</option>
                    </Input>
                  </FormGroup>
                <FormGroup>
                  <label
                    className="form-control-label"
                    htmlFor="input-status"
                  >
                    Status
                  </label>
                  <br />
                  <label className="custom-toggle">
                    <input
                      type="checkbox"
                      defaultChecked
                      name="status"
                      onChange={() => setStatus(!status)}
                    />
                    <span className="custom-toggle-slider rounded-circle"></span>
                  </label>
                </FormGroup>
                <Button color="primary" type="submit">
                  Submit
                </Button>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    )
}

