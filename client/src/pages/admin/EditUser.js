import React, { useState, useEffect } from 'react'

import {
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    Form,
    FormGroup,
    Input,
    Button,
  } from "reactstrap";
import { useDispatch } from 'react-redux';
import { alert } from '../../actions/alert';
import api from '../../utils/api';
import { Redirect, useParams  } from 'react-router-dom';

export default function EditUser() {

    const { id } = useParams();

    const [username,setUsername] = useState("");
    const [email,setEmail] = useState("");
    const [typeOfUser,setTypeOfUser] = useState("");
    const [status,setStatus] = useState(true);
    const [redirect,setRedirect] = useState(false);

    const dispatch = useDispatch();


    useEffect(() => {
        
        (async () => {
            try {
                const res = await api.get("/users/"+id);
                const resData = res.data.user;
                setUsername(resData.username);
                setEmail(resData.email);
                setTypeOfUser(resData.typeOfUser)
                let userStatus = true;
                if(resData.status === "inactive"){
                    userStatus = false;
                }
                setStatus(userStatus);
            } catch (error) {
                console.log(error);
                let msg = error.response.data.error;
                dispatch(alert(msg,'danger'));
            }
        })()

    },[]);


    const handleFormSubmit = async (e) => {
        e.preventDefault();

        try {
            
            let userStatus = 'active'
            if(!status)
                userStatus = 'inactive';

            
            const res = await api.put("/users/"+id,{
                username,
                email,
                typeOfUser,
                status:userStatus
            });
            dispatch(alert("user Updated successfully",'success'));
           setRedirect(true);
        } catch (error) {
            console.log(error.response);
            let msg = error.response.data.error;
            dispatch(alert(msg,'danger'));
        }
    };

    if(redirect){
        return <Redirect to="/admin/users" />
    }

    return (
        <Row className="mt-4">
        <Col>
          <Card className="bg-secondary shadow">
            <CardHeader className="bg-white border-0">
              <h3 className="mb-0">Add User</h3>
            </CardHeader>
            <CardBody>

              <Form className="px-lg-5" onSubmit={handleFormSubmit}>
                <FormGroup>
                  <label className="form-control-label" htmlFor="input-username">
                    Username
                  </label>
                  <Input
                    className="form-control-alternative"
                    placeholder="Username"
                    type="text"
                    name="username"
                    value={username}
                    required
                    onChange={(e) => setUsername(e.target.value)}
                  />
                </FormGroup>
                <FormGroup>
                  <label className="form-control-label" htmlFor="input-email">
                    Email
                  </label>
                  <Input
                    className="form-control-alternative"
                    placeholder="Email"
                    type="email"
                    name="email"
                    value={email}
                    required
                    onChange={(e) => setEmail(e.target.value)}
                  />
                </FormGroup>
                <FormGroup>
                    <label
                      className="form-control-label"
                      htmlFor="input-category"
                    >
                      User Type
                    </label>
                    <Input
                      type="select"
                      onChange={(e) => setTypeOfUser(e.target.value)}
                      className="form-control-alternative"
                      name="typeOfUser"
                    >
                      <option selected={typeOfUser === "business"} value="business">business</option>
                      <option selected={typeOfUser === "main" } value="main">main</option>
                    </Input>
                  </FormGroup>
                <FormGroup>
                  <label
                    className="form-control-label"
                    htmlFor="input-status"
                  >
                    Status
                  </label>
                  <br />
                  <label className="custom-toggle">
                    <input
                      type="checkbox"
                      checked={status}
                      name="status"
                      onChange={() => setStatus(!status)}
                    />
                    <span className="custom-toggle-slider rounded-circle"></span>
                  </label>
                </FormGroup>
                <Button color="primary" type="submit">
                  Update
                </Button>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>
    )
}

