import React, { useState } from "react";
import { 
    Button,
    Card, 
    CardBody, 
    Col, 
    Container, 
    Form, 
    FormGroup, 
    Input, 
    Row,
} from "reactstrap";
import { useDispatch,useSelector } from 'react-redux'
import { Link, Redirect } from 'react-router-dom'

//imports
import { register } from '../actions/user'

export default function Register() {

    const [username,setUsername] = useState("");
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");
    
    const dispatch = useDispatch();
    const { isAuthenticated }  = useSelector(state => state.user);

    const handleSubmit = async (e) => {
        e.preventDefault();
        
        dispatch(register({
            username,
            email,
            password
        }))
        
    }

    //if authenicated redirect to user homepage
    if(isAuthenticated){
        return <Redirect to="/" />
    }


    return (
        <Container>
            <Row className="justify-content-md-center">
                <Col md={{ size: 6 }} className="my-5">
                    <h1 className="display-2 text-center">Register</h1>
                    <Card style={{ width: "100%" }} className="mt-4">
                        <CardBody>
                            <Form onSubmit={handleSubmit}>
                                <FormGroup>
                                    <Input 
                                        className="form-control-alternative"
                                        id="exampleFormControlInput1"
                                        placeholder="Username"
                                        type="text"
                                        onChange={(e) => setUsername(e.target.value)}
                                        required
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Input 
                                        className="form-control-alternative"
                                        id="exampleFormControlInput1"
                                        placeholder="Email"
                                        type="email"
                                        onChange={(e) => setEmail(e.target.value)}
                                        required
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Input 
                                        className="form-control-alternative"
                                        id="exampleFormControlInput1"
                                        placeholder="password"
                                        type="password"
                                        onChange={(e) => setPassword(e.target.value)}
                                        required
                                    />
                                </FormGroup>
                                <FormGroup className="text-center">
                                    <Button color="primary" type="submit">
                                        Register
                                    </Button>
                                </FormGroup>
                            </Form>
                            <p className="text-center">Already have an account ? <Link to="/login">Login</Link></p>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </Container>
    );
}
