import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';
import AdminNavbar from '../Navbar';
import AdminRoute from '../routing/AdminRoute';

//pages
import UsersPage from '../../pages/admin/Users';
import AddUsersPage from '../../pages/admin/AddUser';
import EditUsersPage from '../../pages/admin/EditUser';
import PackagesPage from '../../pages/admin/Packages';
import AddPackagePage from '../../pages/admin/AddPackage';
import EditPackagePage from '../../pages/admin/EditPackage';

export default function adminLayout() {
    return (
        <>
            <AdminNavbar />
            <Container>
                <Switch>
                    <AdminRoute path="/admin/packages/edit/:id" component={EditPackagePage} />
                    <AdminRoute path="/admin/packages/add" component={AddPackagePage} />
                    <AdminRoute path="/admin/packages" component={PackagesPage} />
                    <AdminRoute path="/admin/users/edit/:id" component={EditUsersPage} />
                    <AdminRoute path="/admin/users/add" component={AddUsersPage} />
                    <AdminRoute path="/admin/users" component={UsersPage} />
                    <Redirect from="/" to="/admin/users" />
                </Switch>
            </Container>
        </>
    )
}
