import React, { useEffect, useState } from "react";
import { Pagination, PaginationItem, PaginationLink } from "reactstrap";

export default function CustomPagination({itemPerPage,page,totalRecords,chnagePage}) {

  let totalPages = Math.ceil(totalRecords / itemPerPage);

  //calcuating total page number and store in the array
  const pageNumbers = [];
  for (let i = 1; i <= totalPages; i++) {
    pageNumbers.push(i);
  }

  return (
    <nav aria-label="...">
      <Pagination
        className="pagination justify-content-end mb-0"
        listClassName="justify-content-end mb-0"
      >
        <PaginationItem className={page === 1 ? "disabled" : ""}>
          <PaginationLink
            onClick={(e) => chnagePage(page - 1)}
            tabIndex="-1"
          >
            <i className="fas fa-angle-left" />
            <span className="sr-only">Previous</span>
          </PaginationLink>
        </PaginationItem>

        {pageNumbers.map((number) => {
          return (
            <PaginationItem className={number === page ? "active" : ""}>
              <PaginationLink onClick={(e) => chnagePage(number)}>
                {number}
              </PaginationLink>
            </PaginationItem>
          );
        })}

        <PaginationItem
          className={page === totalPages ? "disabled" : ""}
        >
          <PaginationLink onClick={(e) => chnagePage(page + 1)}>
            <i className="fas fa-angle-right" />
            <span className="sr-only">Next</span>
          </PaginationLink>
        </PaginationItem>
      </Pagination>
    </nav>
  );
}
