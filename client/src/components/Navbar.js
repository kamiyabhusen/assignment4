import React from 'react'
import { Link, NavLink as NavLinkRRD } from 'react-router-dom'
import { 
    Col, 
    Container,  
    Nav, 
    Navbar, 
    NavbarBrand, 
    NavItem, 
    NavLink, 
    Row, 
    UncontrolledCollapse, 
} from 'reactstrap'
import { useDispatch } from 'react-redux';
import { logout  } from '../actions/user'

export default function AdminNavbar() {

    const dispatch = useDispatch();

    //logout button handler
    const handleLogout = () => {
      dispatch(logout());
    }

    return (
        <Navbar
          className="navbar-horizontal navbar-dark bg-primary"
          expand="lg"
        >
          <Container>
            <NavbarBrand href="#pablo" onClick={e => e.preventDefault()}>
              AdminPanel
            </NavbarBrand>
            <button
              aria-controls="navbar-primary"
              aria-expanded={false}
              aria-label="Toggle navigation"
              className="navbar-toggler"
              data-target="#navbar-primary"
              data-toggle="collapse"
              id="navbar-primary"
              type="button"
            >
              <span className="navbar-toggler-icon" />
            </button>
            <UncontrolledCollapse navbar toggler="#navbar-primary">
              <div className="navbar-collapse-header">
                <Row>
                  <Col className="collapse-brand" xs="6">
                    <Link to="/admin">
                      AdminPanel
                    </Link>
                  </Col>
                  <Col className="collapse-close" xs="6">
                    <button
                      aria-controls="navbar-primary"
                      aria-expanded={false}
                      aria-label="Toggle navigation"
                      className="navbar-toggler"
                      data-target="#navbar-primary"
                      data-toggle="collapse"
                      id="navbar-primary"
                      type="button"
                    >
                      <span />
                      <span />
                    </button>
                  </Col>
                </Row>
              </div>
              <Nav className="ml-lg-auto" navbar>
                <NavItem>
                  <NavLink to="/admin/users" tag={NavLinkRRD} >
                    Users
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink to="/admin/packages" tag={NavLinkRRD}>
                    Packages
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink onClick={handleLogout}>
                    Logout
                  </NavLink>
                </NavItem>
              </Nav>
            </UncontrolledCollapse>
          </Container>
        </Navbar>
    )
}
