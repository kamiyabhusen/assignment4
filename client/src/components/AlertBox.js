/**
 * Alert Component for Displaying Alerts
 */

import React from 'react'
import { useSelector } from 'react-redux'

import {
    Alert
} from 'reactstrap'

export default function AlertBox() {

    const { alerts }  = useSelector(state => state.alert);

    return (
        alerts.map((alert) => (
            <div key={alert.id} className="floating-alert">
                <Alert color={alert.alertType}>
                    {alert.msg}
                </Alert>
            </div>
        ))
    )
}
