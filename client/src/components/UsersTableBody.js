import React from "react";
import {NavLink as NavLinkRRD} from 'react-router-dom'

//reactstrap imports
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";

export default function UsersTableBody(props) {


    const handleDeleteBtn = () => {
        
        props.handleDelete(props.user.id);
    }
  return (
    <tr>
      <td>
        <span className="mb-0 text-sm">{props.index}</span>
      </td>
      <td>{props.user.username}</td>
      <td>{props.user.email}</td>
      <td>{props.user.typeOfUser}</td>
      <td>{props.user.status}</td>
      <td>{props.user.userPackage ? props.user.userPackage.name : ''}</td>
      <td className="text-right">
        <UncontrolledDropdown>
          <DropdownToggle
            className="btn-icon-only text-light"
            href="#pablo"
            role="button"
            size="sm"
            color=""
            onClick={(e) => e.preventDefault()}
          >
            <i className="fas fa-ellipsis-v" />
          </DropdownToggle>
          <DropdownMenu className="dropdown-menu-arrow">
            <DropdownItem to={"/admin/users/edit/" + props.user.id} tag={NavLinkRRD}>
              Edit
            </DropdownItem>
            <DropdownItem onClick={handleDeleteBtn}>
              Delete
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </td>
    </tr>
  );
}
