/**
 * Protected Route component for Admin routes
 */

import React from 'react'
import { useSelector } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';

const AdminRoute = ({
    component: Component,
    ...rest
}) => {

    const { loading, isAuthenticated, user } = useSelector(state => state.user);

    return (
        <Route 
        {...rest}
        render={props => 
            !loading && (
                isAuthenticated ? (
                    user.typeOfUser === "main" ? (
                        <Component {...props}/>
                    ) :(
                        <Redirect to="/" />
                    )
                ) : (
                    <Redirect to="/login" />
                )
            )
        }
        />
    )
}

export default AdminRoute;