/**
 * Protected Route component for logged in user routes
 */

import React from 'react'
import { useSelector } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';

const PrivateRoute = ({
    component: Component,
    ...rest
}) => {

    const { loading, isAuthenticated } = useSelector(state => state.user);

    return (
        <Route 
        {...rest}
        render={props => 
            !loading && (
                isAuthenticated ? (
                    <Component {...props}/>
                ) : (
                    <Redirect to="/login" />
                )
            )
        }
        />
    )
}

export default PrivateRoute;