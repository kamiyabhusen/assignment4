import { configureStore } from '@reduxjs/toolkit'
import { combineReducers } from 'redux'

//import reducers
import user from './slices/userSlices';
import alert from './slices/alertSlices';

//All Reducers
const reducer = combineReducers({
    user,
    alert
})

//redux store
const store = configureStore({
    reducer
})

export default store;