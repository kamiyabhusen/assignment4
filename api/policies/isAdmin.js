/**
 * 
 * @description this policy check user is main user or not
 */

module.exports = async function(req,res,next) {
    
    const user = await User.findOne({id:req.user});

    if(user.typeOfUser != "main"){
        return res.status(403).json({error:"Your are not main user"});
    }
    next();
}