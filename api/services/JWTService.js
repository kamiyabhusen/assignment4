/**
 * JWT Service
 *
 * @description :: this service for creating JWT token and Verifying token
 */
const jwt = require("jsonwebtoken");


module.exports = {

    createJWT:function(payload) {
        return jwt.sign(payload,sails.config.secret,{
            expiresIn:"1h"
        });
    },

    verify:function(token) {
        return jwt.verify(token,sails.config.secret);
    },

}