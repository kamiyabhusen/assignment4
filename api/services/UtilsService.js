const { func } = require("joi");
/**
 * Util Service
 *
 * @description :: utils services.
 */


module.exports = {
    //compare date after 14 day added
    compareDate:function(date) {
        let newDate = new Date(date);
        newDate.setDate(newDate.getDate() + 14);

        if(newDate >= new Date())
            return true;
        else
            return false;
    }
}