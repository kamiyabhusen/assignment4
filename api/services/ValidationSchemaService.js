/**
 * validation schema Service
 *
 * @description :: this service for defining the validation schemas
 */

const Joi = require('joi');
module.exports = {
    //create user schema
    createUser: function() {
      
        const schema = Joi.object({
            username:Joi.string()
                .alphanum()
                .min(3)
                .max(30)
                .required(),
            email:Joi.string()
                .email()
                .required(),
            password:Joi.string()
                .min(6)
                .max(30)
                .required(),
            typeOfUser:Joi.string()
                .required()
                .pattern(/(main|business)/),
            package:Joi.string(),
            status:Joi.string()
                .pattern(/(active|inactive)/)
        });
        return schema;
        
    },
    //create user schema
    updateUser: function() {
      
        const schema = Joi.object({
            username:Joi.string()
                .alphanum()
                .min(3)
                .max(30)
                .required(),
            email:Joi.string()
                .email()
                .required(),
            typeOfUser:Joi.string()
                .required()
                .pattern(/(main|business)/),
            package:Joi.string(),
            status:Joi.string()
                .pattern(/(active|inactive)/)
        });
        return schema;
        
    },

    //login input schema
    login: function() {
      
        const schema = Joi.object({
            username:Joi.string()
                .alphanum()
                .min(3)
                .max(30)
                .required(),
            password:Joi.string()
                .min(6)
                .max(30)
                .required()
        });
        return schema;
        
    },

    //create package schema
    createPackage:function() {
        const schema = Joi.object({
            name:Joi.string()
                .required(),
            price:Joi.number()
                .required(),
            status:Joi.string()
                .pattern(/(active|inactive)/)
        });
        return schema;
    }
}