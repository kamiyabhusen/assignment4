/**
 * PackageController
 *
 * @description :: Server-side actions for handling incoming requests for packages.
 */

module.exports = {
  
     /**
     * @route POST /api/packages/
     * @access private Only Main User
     * @description :: Create package
     */
    create:async function(req,res) {
        try {
            
            //validate the req body
            const schema = ValidationSchemaService.createPackage();
            const {error:validationError} = schema.validate(req.body);

            //if validationError define that means error in validation
            if(validationError){
                return res.badRequest({error:validationError.details});
            }

            //creating package in database
            const package = await Package.create(req.body).fetch();


            res.ok({package});
            

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

    /**
     * @route GET /api/packages/
     * @access private 
     * @description :: Get ALL packages
     */
    find:async function(req,res) {
        try {

            //For Pagination
            let perPage = 5,page = 1;

            //if perpage not given in query assign default
            if(req.query.perpage){
                perPage = req.query.perpage;
            }

            //if page not given in query assign default
            if(req.query.page){
                page = req.query.page;
            }

            //count total number of Package
            const totalPackage = await Package.count({});
            
            
            //get all packages based on page number and perpage 
            const packages = await Package.find({
                skip: perPage * (page -1),
                limit: perPage
            });
            
            res.ok({
                packages,
                total:totalPackage,
                page,
                perPage
            });

        } catch (error) {

            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

    /**
     * @route GET /api/packages/:id
     * @access private 
     * @description :: GET specific package
     */
    findOne:async function(req,res) {
        try {
            //get id form params
            const id = req.param('id');
            //get package by id
            const package = await Package.findOne({id});

            //if there is no package
            if(!package){
                return res.status(404).json({error:"No Package"});
            }
            
            res.ok({package});

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

    /**
     * @route PUT /api/packages/:id
     * @access private Only Main User
     * @description :: Update specific package
     */
    update:async function(req,res) {
        try {
            //get id form params
            const id = req.param('id');

            //validate the req body
            const schema = ValidationSchemaService.createPackage();
            const {error:validationError} = schema.validate(req.body);

            //if validationError define that means error in validation
            if(validationError){
                return res.badRequest({error:validationError.details});
            }
            
            //update the records by id
            const package = await Package.updateOne({id}).set(req.body);

            res.ok({package});
            

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

    /**
     * @route DELETE /api/packages/:id
     * @access private Only Main User
     * @description :: delete specific package
     */
    delete:async function(req,res) {
        try {
            //get id form params
            const id = req.param('id');

            //delte the records by id
            const package = await Package.destroyOne({id});

            res.ok({package});
            

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },
};

