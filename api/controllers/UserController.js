/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 */


module.exports = {


    /**
     * @route POST /api/users
     * @access public
     * @description :: to Create user(register)
     */
    create:async function(req,res) {

        try {
            //validation the req.body
            const schema = ValidationSchemaService.createUser()
            const { value, error:validationError } = schema.validate(req.body);
            
            //if validationError define that means error in validation
            if(validationError){
                return res.badRequest({error:validationError.details[0].message});
            }
    
            //check username is already exist
            const isUsernameExist = await User.findOne({username:req.body.username});
            if(isUsernameExist){
                return res.badRequest({error:"Username is already exist."});
            }

            //check email is already exist
            const isEmailExist = await User.findOne({email:req.body.email});
            if(isEmailExist){
                return res.badRequest({error:"email is already exist."});
            }

            //encrypt the passowrd
            const encryptedPassword = await PasswordService.hashPassword(req.body.password);

            let userPackage = null;           
            //if typeofuser is bussiness assign trial pack
            if(req.body.typeOfUser === "business"){
                const trailPack = await Package.findOne({name:'Trail Pack'});
                userPackage = trailPack.id;
            }

            //if status not given set default value
            let status;
            if(!req.body.status){
                status = 'active';
            }

            //create user in database
            const createdUser = await User.create({
                ...req.body,
                userPackage,
                status,
                password:encryptedPassword
            }).fetch();

            //generating token
            const token = JWTService.createJWT({
                id:createdUser.id
            })

            //returning token and username, typeofuser
            res.ok({
                token,
                username:createdUser.username,
                typeOfUser:createdUser.typeOfUser
            });
            
        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }

    },

     /**
     * @route POST /api/users/login
     * @access public
     * @description :: User Login function
     */
    login:async function(req,res) {
        try {
            //validation the req.body
            const schema = ValidationSchemaService.login()
            const { value, error:validationError } = schema.validate(req.body);

            //if validationError define that means error in validation
            if(validationError){
                return res.badRequest({error:validationError.details[0].message});
            }

            //check username is correct or not
            const user = await User.findOne({username:req.body.username});
            if(!user){
                return res.badRequest({error:"Username is invalid"});
            }

            //comparing passwords
            const matchedPassword = await PasswordService.comparePassword(req.body.password,user.password);
            if(!matchedPassword){
                return res.badRequest({error:"Password is invalid"});
            }

            //generating token
            const token = JWTService.createJWT({
                id:user.id
            })

            //returning token and username
            res.ok({
                token,
                username:user.username,
                typeOfUser:user.typeOfUser
            });

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

    /**
     * @route GET /api/users/me
     * @access private 
     * @description :: get current user details
     */
    me:async function(req,res) {
        try {
            
            //get current user
            const user = await User.findOne({id:req.user});
            
            res.ok({
                username:user.username,
                typeOfUser:user.typeOfUser
            });

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

    /**
     * @route GET /api/users
     * @access private only Main User
     * @description :: get All Business user
     */
    find:async function(req,res) {
        try {
            //For Pagination
            let perPage = 5,page = 1;

            //if perpage not given in query assign default
            if(req.query.perpage){
                perPage = req.query.perpage;
            }

            //if page not given in query assign default
            if(req.query.page){
                page = req.query.page;
            }

            //count total number of Package
            const totalUsers = await User.count({});

            //get all users based on page number and perpage
            const users = await User.find({
                skip: perPage * (page -1),
                limit: perPage
            }).populate("userPackage");
            
            res.ok({
                users,
                total:totalUsers,
                page,
                perPage
            });

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

    /**
     * @route GET /api/users/package
     * @access private 
     * @description :: get information about package if not expired
     */
    getPackage:async function(req,res) {
        try {
            
            //get Current user with its package
            const user = await User.findOne({
                where:{id:req.user},
                omit: ['password']
            }).populate("userPackage");

            //checking for trail package expried or not
            if(user.userPackage.name === "Trail Pack" && !UtilsService.compareDate(user.createdAt)){
                await User.updateOne({id:req.user}).set({
                    status:'inactive'
                })
                return res.badRequest({error:"Trail Pack is Expired"});
            }

            res.ok({
                name:user.userPackage.name,
                createdAt:user.createdAt
            });

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

    /**
     * @route GET /api/users/:id
     * @access private 
     * @description :: get information about specific user
     */
    findOne:async function(req,res) {
        try {
            //get id form params
            const id = req.param('id');
            //get package by id
            const user = await User.findOne({id});

            //if there is no package
            if(!user){
                return res.status(404).json({error:"No User"});
            }
            
            res.ok({user});

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

    /**
     * @route PUT /api/users/:id
     * @access private 
     * @description :: Update information about specific user
     */
    update:async function(req,res) {
        try {
            //get id form params
            const id = req.param('id');

            //validate the req body
            const schema = ValidationSchemaService.updateUser();
            const {error:validationError} = schema.validate(req.body);

            //if validationError define that means error in validation
            if(validationError){
                return res.badRequest({error:validationError.details[0].message});
            }

            
            //update the records by id
            const user = await User.updateOne({id}).set(req.body);

            res.ok({user});
            

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

    /**
     * @route Delete /api/users/:id
     * @access private 
     * @description :: Delete specific user
     */
    delete:async function(req,res) {
        try {
            //get id form params
            const id = req.param('id');

            //delte the records by id
            const user = await User.destroyOne({id});

            res.ok({user});
            

        } catch (error) {
            console.log(error.message);
            //Return 500 if any error happend
            return res.serverError({error:"Internal Server Error"})
        }
    },

};

