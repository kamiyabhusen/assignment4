/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

    //user Routes
    'POST /api/users':'UserController.create',
    'POST /api/users/login':'UserController.login',
    'GET /api/users':'UserController.find',
    'GET /api/users/me':'UserController.me',
    'GET /api/users/package':'UserController.getPackage',
    'GET /api/users/:id':'UserController.findOne',
    'PUT /api/users/:id':'UserController.update',
    'DELETE /api/users/:id':'UserController.delete',


    //Package Routes
    'POST /api/packages':'PackageController.create',
    'GET /api/packages':'PackageController.find',
    'GET /api/packages/:id':'PackageController.findOne',
    'PUT /api/packages/:id':'PackageController.update',
    'DELETE /api/packages/:id':'PackageController.delete',
};

